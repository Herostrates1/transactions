/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.dataaccess;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author pbirg
 */
@Entity
public class UserEntity {
    
    public UserEntity() {}
    
    public UserEntity(String id) {
        this.id = id;
    }

    @Id
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
}
