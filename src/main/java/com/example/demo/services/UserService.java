/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.services;

import com.example.demo.dataaccess.UserEntity;
import com.example.demo.dataaccess.UserRepository;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author pbirg
 */
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    // this rollback will work 
    @Transactional
    public void simpleTransaction() {
        userRepository.save(new UserEntity(UUID.randomUUID().toString()));
        userRepository.save(new UserEntity(UUID.randomUUID().toString()));
        userRepository.save(new UserEntity(UUID.randomUUID().toString()));
        System.out.println("Before exception is thrown: " + userRepository.findAll().size());
        throw new RuntimeException();
    }
    
    
    // this rollback won't work 
    // Michal's case 
    @Transactional
    public void caughtUncheckedExceptionInsideTransaction() {
        try {
            userRepository.save(new UserEntity(UUID.randomUUID().toString()));
            userRepository.save(new UserEntity(UUID.randomUUID().toString()));
            userRepository.save(new UserEntity(UUID.randomUUID().toString()));
            System.out.println("Before exception is thrown: " + userRepository.findAll().size());
            throw new RuntimeException();
        } catch (RuntimeException e) {
            System.out.println("RunTimeException przechwycony");
        }
    }

    // this rollback won't work 
    @Transactional
    public void simpleTransactionWithException() throws Exception {
        userRepository.save(new UserEntity(UUID.randomUUID().toString()));
        userRepository.save(new UserEntity(UUID.randomUUID().toString()));
        userRepository.save(new UserEntity(UUID.randomUUID().toString()));
        System.out.println("Before exception is thrown: " + userRepository.findAll().size());
        throw new Exception();
    }

    // this rollback will work 
    @Transactional(rollbackFor = Exception.class)
    public void simpleTransactionWithException2() throws Exception {
        userRepository.save(new UserEntity(UUID.randomUUID().toString()));
        userRepository.save(new UserEntity(UUID.randomUUID().toString()));
        userRepository.save(new UserEntity(UUID.randomUUID().toString()));
        System.out.println("Before exception is thrown: " + userRepository.findAll().size());
        throw new Exception();
    }


}
