/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.controllers;

import com.example.demo.dataaccess.UserEntity;
import com.example.demo.dataaccess.UserRepository;
import com.example.demo.services.UserService;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author pbirg
 */
@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private UserService userService;

    @GetMapping("/caughtUncheckedExceptionInsideTransaction")
    private String caughtUncheckedExceptionInsideTransaction() {
        userService.caughtUncheckedExceptionInsideTransaction();
        return "done";
    }

    @GetMapping("/simpleTransaction")
    private String simple() {
        userService.simpleTransaction();
        return "done";
    }

    @GetMapping("/simpleTransactionWithException")
    private String simpleTransactionWithException() throws Exception {
        userService.simpleTransactionWithException();
        return "done";
    }

    @GetMapping("/simpleTransactionWithException2")
    private String simpleTransactionWithException2() throws Exception {
        userService.simpleTransactionWithException2();
        return "done";
    }

    @GetMapping("/count")
    private String count() {
        return String.valueOf(userRepository.findAll().size());
    }

    @GetMapping("/delete")
    private String delete() {
        userRepository.deleteAll();
        return "ok";
    }

    @GetMapping("/save")
    private String save() {
        userRepository.save(new UserEntity(UUID.randomUUID().toString()));
        return "ok";
    }
}
